package process

// Conf - параметры
type Conf struct {
	File, Args                     string // Файл и аргументы процесса
	InFile, OutFile, ErrFile       string // Файлы-копии
	InChSize, OutChSize, ErrChSize int    // Размеры каналов
	LogName                        string
	LogFile                        string
	logLevel                       int
}
