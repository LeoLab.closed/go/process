package process

import (
	"bufio"
	"errors"
	"io"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/LeoLab/go/leotools"
)

// Версия
const (
	Version = "0.0.2"
	Date    = "02/07/2018"
	MName   = "LeoLab/process"
)

// Proc -
type Proc struct {
	Err            error
	cfg            *Conf
	STDIN          chan string
	STDOUT, STDERR chan string
	run            chan bool
	proc           *exec.Cmd
	log            *leotools.Logger

	fProcIn  io.WriteCloser
	fProcOut io.ReadCloser
	fProcErr io.ReadCloser
}

// New - Создать экземпляр
func New(c *Conf) (p *Proc, err error) {
	p = &Proc{
		//		STDOUT: make(chan string, c.OutChSize),
		//		STDERR: make(chan string, c.ErrChSize),
		//		STDIN:  make(chan string, c.InChSize),
		cfg: c,
	}
	args := strings.Split(c.Args, " ")
	p.proc = exec.Command(c.File, args...)

	return p, nil
}

// Start -
func (p *Proc) Start() (err error) {
	const f = "Start"
	p._dbg(f, "Enter")
	defer func() { p._dbg(f, "Leave") }()
	p.run = make(chan bool)

	//= Получение пайпов >
	//= Получение пайпа STDIN процесса >
	p.fProcIn, err = p.proc.StdinPipe()
	if err != nil {
		p._err(f, "Невозможно получить STDIN процесса: %s", err.Error())
		return
	}
	//< Получение пайпа STDIN процесса =
	if p.fProcOut, err = p.proc.StdoutPipe(); err != nil {
		p._err(f, "Невозможно получить STDOUT процесса: %s", err.Error())
		return
	}
	if p.fProcErr, err = p.proc.StderrPipe(); err != nil {
		p._err(f, "Невозможно полученить STDERR процесса: %s", err.Error())
		return
	}
	//< Получение пайпов =

	go p.stdin()
	go p.stdout()
	go p.stderr()

	if err = p.proc.Start(); err != nil {
		p._err(f, "Ошибка запуска процесса: %s", err.Error())
		close(p.run)
		return errors.New("Ошибка запуска процесса: " + err.Error())
	}
	go p.wait()
	p._info(f, "Process started")
	return nil
}

// Stop -
func (p *Proc) Stop() (err error) {
	const f = "Stop"
	p._dbg(f, "Enter")
	defer func() { p._dbg(f, "Leave") }()

	if p.proc == nil {
		p._err(f, "Попытка остановить остановленный процесс.")
		return errors.New("Процесс уже остановлен")
	}

	if err = p.proc.Process.Signal(os.Interrupt); err != nil {
		p._err(f, "Ошибка отправки сигнала остановки процесса: %s", err.Error())
		return errors.New("Ошибка остановки процесса: " + err.Error())
	}
	p._info(f, "Ожидание завершения процесса...")
	<-p.run
	p._info(f, "Процесс завершен.")
	return nil
}

func (p *Proc) wait() {
	const f = "wait"
	p._dbg(f, "Enter")
	defer func() { p._dbg(f, "Leave") }()
	err := p.proc.Wait()
	if err != nil {
		p._err(f, "Процесс завершился с ошибкой: %s", err.Error())
		p.Err = err
	}
	p._info(f, "Статус завершения процесса: %s, UserTime: %s, SystemTime: %s", p.proc.ProcessState.String, p.proc.ProcessState.UserTime().String(), p.proc.ProcessState.SystemTime().String())
	close(p.run)
}

func (p *Proc) stdin() {
	const f = "stdin"
	var fIn *os.File
	//	var
	var err error
	p._dbg(f, "Enter")
	p.STDIN = make(chan string, p.cfg.InChSize)
	defer func() { close(p.STDIN); p._dbg(f, "Leave") }()

	//= Открытие файла-копии >
	if p.cfg.InFile != "" {
		if fIn, err = os.OpenFile(p.cfg.InFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666); err != nil {
			p._err(f, "Ошибка открытия файла-копии STDIN процесса: %s", err.Error())
			return
		}
		defer func() {
			if err = fIn.Close(); err != nil {
				p._err(f, "Ошибка закрытия файла-копии STDIN процесса: %s", err.Error())
			}
		}()
	}
	//< Открытие файла-копии =

	for {
		select {
		case s := <-p.STDIN:
			p._dbg(f, "Received: {%s}", s)
			if fIn != nil {
				if _, err = fIn.Write([]byte(s + "\n")); err != nil {
					p._err(f, "Ошибка записи в файл-копию STDIN процесса: %s", err.Error())
					return
				}
			}
			if _, err = p.fProcIn.Write([]byte(s + "\n")); err != nil {
				p._err(f, "Ошибка записи в STDIN процесса: %s", err.Error())
				return
			}
		case <-p.run:
			p._dbg(f, "Завершение процесса")
			return
		}
	}
}

func (p *Proc) stdout() {
	const f = "stdout"
	var fOut *os.File
	//	var fProcO io.ReadCloser
	var fProcOut *bufio.Scanner
	var err error
	p._dbg(f, "Enter")
	p.STDOUT = make(chan string, p.cfg.OutChSize)
	defer func() { close(p.STDOUT); p._dbg(f, "Leave") }()

	fProcOut = bufio.NewScanner(p.fProcOut)
	//= Открытие файла-копии >
	if p.cfg.OutFile != "" {
		if fOut, err = os.OpenFile(p.cfg.OutFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666); err != nil {
			p._err(f, "Ошибка открытия файла-копии STDOUT процесса: %s", err.Error())
			return
		}
		defer func() {
			if err = fOut.Close(); err != nil {
				p._err(f, "Ошибка закрытия файла-копии STDOUT процесса: %s", err.Error())
			}
		}()
	}
	//< Открытие файла-копии =

	for fProcOut.Scan() {
		s := fProcOut.Text()
		p._dbg(f, "Received: {%s}", s)
		if fOut != nil {
			if _, err = fOut.WriteString(s + "\n"); err != nil {
				p._err(f, "Ошибка записи в файл-копию STDOUT процесса: %s", err.Error())
				return
			}
		}
		select {
		case p.STDOUT <- s:
			// Отправили в пайп
		default:
			p._err(f, "Ошибка записи в канал STDOUT")
			return
		}
	}
	if err = fProcOut.Err(); err != nil {
		p._err(f, "Ошибка чтения STDOUT процесса: %s", err.Error())
	}

}

func (p *Proc) stderr() {
	const f = "stderr"
	var fErr *os.File
	//	var fProcE io.ReadCloser
	var fProcErr *bufio.Scanner
	var err error
	p._dbg(f, "Enter")
	p.STDERR = make(chan string, p.cfg.ErrChSize)
	defer func() { close(p.STDERR); p._dbg(f, "Leave") }()

	fProcErr = bufio.NewScanner(p.fProcErr)
	//= Открытие файла-копии >
	if p.cfg.ErrFile != "" {
		if fErr, err = os.OpenFile(p.cfg.ErrFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666); err != nil {
			p._err(f, "Ошибка открытия файла-копии STDERR процесса: %s", err.Error())
			return
		}
		defer func() {
			if err = fErr.Close(); err != nil {
				p._err(f, "Ошибка закрытия файла-копии STDERR процесса: %s", err.Error())
			}
		}()
	}
	//< Открытие файла-копии =
	for fProcErr.Scan() {
		s := fProcErr.Text()
		p._dbg(f, "Received: {%s}", s)
		if fErr != nil {
			if _, err = fErr.WriteString(s); err != nil {
				p._err(f, "Ошибка записи в файл-копию STDERR процесса: %s", err.Error())
				return
			}
		}
		select {
		case p.STDERR <- s:
			// Отправили в пайп
		default:
			p._err(f, "Ошибка записи в канал STDERR")
			return
		}
	}
	if err = fProcErr.Err(); err != nil {
		p._err(f, "Ошибка чтения STDERR процесса: %s", err.Error())
	}
}
