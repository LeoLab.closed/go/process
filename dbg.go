package process

import (
	"fmt"

	"gitlab.com/LeoLab/go/leotools"
)

// Уровни логгировниая
const (
	LOff  = 0
	LErr  = 1
	LWarn = 2
	LInfo = 3
	LNote = 4
	LAll  = 5
)

// SetLog - включить отладочные сообщения
func (p *Proc) SetLog(lvl int) (err error) {
	p.cfg.logLevel = lvl
	if p.log == nil {
		if p.log, err = leotools.NewLogger(&leotools.LogConf{RCDepth: 3, File: p.cfg.LogFile, Name: p.cfg.LogName}); err != nil {
			return err
		}
	}
	return nil
}

func (p *Proc) _dbg(f, m string, s ...interface{}) {
	if p.cfg.logLevel > 4 {
		if p.log != nil {
			p.log.Debug("[" + MName + "." + f + "] " + fmt.Sprintf(m, s...))
		}
	}
}

func (p *Proc) _log(f, m string, s ...interface{}) {
	if p.cfg.logLevel > 3 {
		if p.log != nil {
			p.log.Info("[" + MName + "." + f + "] " + fmt.Sprintf(m, s...))
		}
	}
}

func (p *Proc) _info(f, m string, s ...interface{}) {
	if p.cfg.logLevel > 2 {
		if p.log != nil {
			p.log.Info("[" + MName + "." + f + "] " + fmt.Sprintf(m, s...))
		}
	}
}

func (p *Proc) _warn(f, m string, s ...interface{}) {
	if p.cfg.logLevel > 1 {
		if p.log != nil {
			p.log.Warning("[" + MName + "." + f + "] " + fmt.Sprintf(m, s...))
		}
	}
}

func (p *Proc) _err(f, m string, s ...interface{}) {
	if p.cfg.logLevel > 0 {
		if p.log != nil {
			p.log.Err("[" + MName + "." + f + "] " + fmt.Sprintf(m, s...))
		}
	}
}
